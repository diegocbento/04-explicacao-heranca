
public class App {
	public static void main(String[] args) {
		Aluno pessoa = new Aluno("João", "123.123.123-12", "123");
		Responsavel responsavel = new Responsavel("José", "321.321.321-32");
		Coordenador coordenador = new Coordenador("Pedro", "123.432.123-12", "4242387423");
//		Pessoa pessoa = new Pessoa("", "");
		
		responsavel.filhos = new Aluno[1];
		responsavel.filhos[0] = pessoa;
		System.out.println(coordenador.getNome());
		System.out.println(coordenador.getCpf());
		System.out.println(coordenador.getFuncional());
		System.out.println(coordenador.dataNascimento);
		
	}
}
